<?php

use Illuminate\Database\Seeder;

class HomeSeeder extends Seeder
{
    public function run()
    {
        DB::table('home')->insert([
            'imagem_1' => '',
            'imagem_2' => '',
            'titulo' => '',
            'subtitulo' => '',
            'texto' => '',
            'imagem_servicos' => '',
            'titulo_servicos' => '',
            'texto_servicos' => '',
        ]);
    }
}
