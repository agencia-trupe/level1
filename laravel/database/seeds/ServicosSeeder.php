<?php

use Illuminate\Database\Seeder;

class ServicosSeeder extends Seeder
{
    public function run()
    {
        DB::table('servicos')->insert([
            'texto' => '',
            'imagem' => '',
            'construcao_reforma' => '',
            'demolicao' => '',
            'ar_condicionado' => '',
            'dry_wall' => '',
            'civil' => '',
            'hidraulica' => '',
            'impermeabilizacao' => '',
            'revestimentos_em_geral' => '',
            'eletrica' => '',
            'pintura' => '',
            'limpeza_fina' => '',
        ]);
    }
}
