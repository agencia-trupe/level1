<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEquipeTable extends Migration
{
    public function up()
    {
        Schema::create('equipe', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->string('titulo');
            $table->text('texto');
            $table->boolean('destaque');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('equipe');
    }
}
