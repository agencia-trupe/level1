<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicosTable extends Migration
{
    public function up()
    {
        Schema::create('servicos', function (Blueprint $table) {
            $table->increments('id');
            $table->text('texto');
            $table->string('imagem');
            $table->text('construcao_reforma');
            $table->text('demolicao');
            $table->text('ar_condicionado');
            $table->text('dry_wall');
            $table->text('civil');
            $table->text('hidraulica');
            $table->text('impermeabilizacao');
            $table->text('revestimentos_em_geral');
            $table->text('eletrica');
            $table->text('pintura');
            $table->text('limpeza_fina');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('servicos');
    }
}
