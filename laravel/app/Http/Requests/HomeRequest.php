<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class HomeRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'imagem_1' => 'image',
            'imagem_2' => 'image',
            'titulo' => 'required',
            'subtitulo' => 'required',
            'texto' => 'required',
            'imagem_servicos' => 'image',
            'titulo_servicos' => 'required',
            'texto_servicos' => 'required',
        ];
    }
}
