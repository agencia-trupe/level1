<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EquipeRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'imagem' => 'required|image',
            'titulo' => 'required',
            'texto' => 'required',
            'destaque' => '',
        ];

        if ($this->method() != 'POST') {
            $rules['imagem'] = 'image';
        }

        return $rules;
    }
}
