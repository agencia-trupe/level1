<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class QuemSomosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'texto' => 'required',
            'imagem' => 'image',
        ];
    }
}
