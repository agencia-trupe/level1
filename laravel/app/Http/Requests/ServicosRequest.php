<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ServicosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'texto' => 'required',
            'imagem' => 'image',
            'construcao_reforma' => 'required',
            'demolicao' => 'required',
            'ar_condicionado' => 'required',
            'dry_wall' => 'required',
            'civil' => 'required',
            'hidraulica' => 'required',
            'impermeabilizacao' => 'required',
            'revestimentos_em_geral' => 'required',
            'eletrica' => 'required',
            'pintura' => 'required',
            'limpeza_fina' => 'required',
        ];
    }
}
