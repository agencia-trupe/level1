<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Projeto;

class ProjetosController extends Controller
{
    public function index()
    {
        $projetos = Projeto::ordenados()->get();

        return view('frontend.projetos', compact('projetos'));
    }
}
