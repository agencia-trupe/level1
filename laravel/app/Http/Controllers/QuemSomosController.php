<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\QuemSomos;
use App\Models\Equipe;

class QuemSomosController extends Controller
{
    public function index()
    {
        $quemSomos = QuemSomos::first();
        $equipe    = [
            'destaque' => Equipe::where('destaque', 1)->ordenados()->get(),
            'normal'   => Equipe::where('destaque', 0)->ordenados()->get()
        ];

        return view('frontend.quem-somos', compact('quemSomos', 'equipe'));
    }
}
