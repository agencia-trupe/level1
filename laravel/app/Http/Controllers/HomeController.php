<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\NewsletterRequest;
use App\Models\Banner;
use App\Models\Home;
use App\Models\Projeto;
use App\Models\Newsletter;

class HomeController extends Controller
{
    public function index()
    {
        $banners  = Banner::ordenados()->get();
        $home     = Home::first();
        $projetos = Projeto::where('destaque', 1)->ordenados()->get();

        return view('frontend.home', compact('banners', 'home', 'projetos'));
    }

    public function newsletter(NewsletterRequest $request)
    {
        Newsletter::create($request->all());

        return response()->json([
            'message' => 'Cadastro efetuado com sucesso!'
        ]);
    }
}
