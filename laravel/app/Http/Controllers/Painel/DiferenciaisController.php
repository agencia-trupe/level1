<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\DiferenciaisRequest;
use App\Http\Controllers\Controller;

use App\Models\Diferencial;

class DiferenciaisController extends Controller
{
    public function index()
    {
        $registros = Diferencial::ordenados()->get();

        return view('painel.diferenciais.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.diferenciais.create');
    }

    public function store(DiferenciaisRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Diferencial::upload_imagem();

            Diferencial::create($input);

            return redirect()->route('painel.diferenciais.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Diferencial $registro)
    {
        return view('painel.diferenciais.edit', compact('registro'));
    }

    public function update(DiferenciaisRequest $request, Diferencial $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Diferencial::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.diferenciais.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Diferencial $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.diferenciais.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
