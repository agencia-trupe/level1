<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;

use App\Models\Newsletter;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;

class NewsletterController extends Controller
{
    public function index()
    {
        $registros = Newsletter::ordenados()->paginate(30);

        return view('painel.newsletter.index', compact('registros'));
    }

    public function destroy(Newsletter $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.newsletter.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

    public function exportar()
    {
        $registros = Newsletter::ordenados()->get([
            'created_at as data', 'email as e-mail'
        ])->each(function ($model) {
            $model->data = Carbon::parse($model->data)->format('d/m/Y H:i');
        });

        $fileName = 'level1-newsletter_'.date('d-m-Y_H-i');

        Excel::create($fileName, function ($excel) use ($registros) {
            $excel->sheet('newsletter', function ($sheet) use ($registros) {
                $sheet->fromModel($registros);
            });
        })->download('xls');
    }
}
