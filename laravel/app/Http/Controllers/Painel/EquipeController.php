<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\EquipeRequest;
use App\Http\Controllers\Controller;

use App\Models\Equipe;

class EquipeController extends Controller
{
    public function index()
    {
        $registros = Equipe::orderBy('destaque', 'DESC')->ordenados()->get();

        return view('painel.equipe.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.equipe.create');
    }

    public function store(EquipeRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Equipe::upload_imagem();

            Equipe::create($input);

            return redirect()->route('painel.equipe.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Equipe $registro)
    {
        return view('painel.equipe.edit', compact('registro'));
    }

    public function update(EquipeRequest $request, Equipe $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Equipe::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.equipe.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Equipe $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.equipe.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
