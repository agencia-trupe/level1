<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Servicos extends Model
{
    protected $table = 'servicos';

    protected $guarded = ['id'];

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 475,
            'height' => 475,
            'path'   => 'assets/img/servicos/'
        ]);
    }

}
