<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Equipe extends Model
{
    protected $table = 'equipe';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            [
                'width'  => 120,
                'height' => 120,
                'upsize' => true,
                'color'  => '#EEEFEF',
                'path'   => 'assets/img/equipe/'
            ],
            [
                'width'  => 260,
                'height' => 260,
                'path'   => 'assets/img/equipe/destaque/'
            ]
        ]);
    }
}
