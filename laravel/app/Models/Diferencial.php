<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Diferencial extends Model
{
    protected $table = 'diferenciais';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 212,
            'height' => 212,
            'upsize' => true,
            'color'  => '#EEEFEF',
            'path'   => 'assets/img/diferenciais/'
        ]);
    }
}
