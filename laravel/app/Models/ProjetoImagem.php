<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class ProjetoImagem extends Model
{
    protected $table = 'projetos_imagens';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeProjeto($query, $id)
    {
        return $query->where('projeto_id', $id);
    }

    public static function uploadImagem()
    {
        return CropImage::make('imagem', [
            [
                'width'   => 180,
                'height'  => 180,
                'path'    => 'assets/img/projetos/imagens/thumbs/'
            ],
            [
                'width'   => 900,
                'height'  => null,
                'upsize'  => false,
                'path'    => 'assets/img/projetos/imagens/'
            ]
        ]);
    }
}
