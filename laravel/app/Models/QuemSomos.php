<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class QuemSomos extends Model
{
    protected $table = 'quem_somos';

    protected $guarded = ['id'];

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 460,
            'height' => null,
            'path'   => 'assets/img/quem-somos/'
        ]);
    }

}
