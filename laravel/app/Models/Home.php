<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Home extends Model
{
    protected $table = 'home';

    protected $guarded = ['id'];

    public static function upload_imagem_1()
    {
        return CropImage::make('imagem_1', [
            'width'  => 300,
            'height' => 300,
            'path'   => 'assets/img/home/'
        ]);
    }

    public static function upload_imagem_2()
    {
        return CropImage::make('imagem_2', [
            'width'  => 300,
            'height' => 300,
            'path'   => 'assets/img/home/'
        ]);
    }

    public static function upload_imagem_servicos()
    {
        return CropImage::make('imagem_servicos', [
            'width'  => 210,
            'height' => null,
            'path'   => 'assets/img/home/'
        ]);
    }

}
