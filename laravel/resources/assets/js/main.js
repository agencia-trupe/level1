import AjaxSetup from './AjaxSetup';
import MobileToggle from './MobileToggle';

AjaxSetup();
MobileToggle();

$('#form-newsletter').submit(function(event) {
    event.preventDefault();

    var $form = $(this);

    if ($form.hasClass('sending')) return false;

    $form.addClass('sending');

    $.ajax({
        type: 'POST',
        url: $form.attr('action'),
        data: {
            email: $form.find('input[name=newsletter_email]').val(),
        }
    }).done(function(data) {
        alert(data.message);
        $form[0].reset();
    }).fail(function(data) {
        var res = data.responseJSON,
            txt = res.email;
        alert(txt);
    }).always(function() {
        $form.removeClass('sending');
    });
});

$('.fancybox').fancybox({
    afterLoad: function() {
        this.title = '<h1>' + $(this.element).data('titulo') + '</h1><p>' + $(this.element).data('descricao') + '</p>';
    },
    padding: 10,
    helpers: {
        title: { type: 'inside' }
    }
});

$('.projetos-thumbs a').click(function(e) {
    e.preventDefault();

    var galeria = $(this).data('galeria');

    $('.hidden a[rel=' + galeria + ']').first().trigger('click');
});

$('.banners').cycle({
    slides: '>.slide',
    pager: '.cycle-pager',
    pagerTemplate: '<a href="#">{{slideNum}}</a>'
});

$('#infografico .clickable').click(function(e) {
    if ($(this).hasClass('active')) return;

    $('#infografico .clickable').removeClass('active');
    $(this).addClass('active');

    $('.circulo-texto h4').html($(this).data('titulo'));
    $('.circulo-texto p').html($(this).data('texto'));
});
