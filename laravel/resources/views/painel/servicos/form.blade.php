@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('texto', 'Texto') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem') !!}
    @if($registro->imagem)
    <img src="{{ url('assets/img/servicos/'.$registro->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<hr>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('construcao_reforma', 'Construção & Reforma') !!}
            {!! Form::textarea('construcao_reforma', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('demolicao', 'Demolição') !!}
            {!! Form::textarea('demolicao', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('ar_condicionado', 'Ar Condicionado') !!}
            {!! Form::textarea('ar_condicionado', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('dry_wall', 'Dry Wall') !!}
            {!! Form::textarea('dry_wall', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('civil', 'Civil') !!}
            {!! Form::textarea('civil', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('hidraulica', 'Hidráulica') !!}
            {!! Form::textarea('hidraulica', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('impermeabilizacao', 'Impermeabilização') !!}
            {!! Form::textarea('impermeabilizacao', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('revestimentos_em_geral', 'Revestimentos em Geral') !!}
            {!! Form::textarea('revestimentos_em_geral', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('eletrica', 'Elétrica') !!}
            {!! Form::textarea('eletrica', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('pintura', 'Pintura') !!}
            {!! Form::textarea('pintura', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('limpeza_fina', 'Limpeza Fina') !!}
            {!! Form::textarea('limpeza_fina', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
