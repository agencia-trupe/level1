@include('painel.common.flash')

<div class="well form-group">
    {!! Form::label('imagem_1', 'Imagem 1') !!}
    @if($registro->imagem_1)
    <img src="{{ url('assets/img/home/'.$registro->imagem_1) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('imagem_1', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem_2', 'Imagem 2') !!}
    @if($registro->imagem_2)
    <img src="{{ url('assets/img/home/'.$registro->imagem_2) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('imagem_2', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('subtitulo', 'Subtítulo') !!}
    {!! Form::textarea('subtitulo', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto', 'Texto') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<hr>

<div class="well form-group">
    {!! Form::label('imagem_servicos', 'Imagem Serviços') !!}
    @if($registro->imagem_servicos)
    <img src="{{ url('assets/img/home/'.$registro->imagem_servicos) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('imagem_servicos', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo_servicos', 'Título Serviços') !!}
    {!! Form::text('titulo_servicos', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto_servicos', 'Texto Serviços') !!}
    {!! Form::textarea('texto_servicos', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
