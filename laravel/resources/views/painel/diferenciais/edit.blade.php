@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Diferenciais /</small> Editar Diferencial</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.diferenciais.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.diferenciais.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
