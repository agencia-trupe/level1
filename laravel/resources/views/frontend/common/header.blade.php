    <header class="desktop @if(Tools::routeIs('home')) home @endif">
        <div class="barra-superior">
            <div class="center">
                <div class="social">
                    @foreach(['instagram', 'facebook', 'linkedin'] as $s)
                        @if($contato->{$s})
                        <a href="{{ $contato->{$s} }}" target="_blank" class="{{ $s }}">{{ $s }}</a>
                        @endif
                    @endforeach
                </div>
                @if($contato->telefone)
                <p class="telefone">{{ $contato->telefone }}</p>
                @endif
                @if($contato->whatsapp)
                <p class="whatsapp">
                    <a href="https://api.whatsapp.com/send?phone=55{{ preg_replace('/\D/', '', $contato->whatsapp) }}" target="_blank">
                    {{ $contato->whatsapp }}
                    </a>
                </p>
                @endif
                <a href="mailto:{{ $contato->email }}" class="email">{{ $contato->email }}</a>
            </div>
        </div>
        <nav>
            <div class="center">
                <a href="{{ route('home') }}" class="logo">{{ config('app.name') }}</a>
                @include('frontend.common.nav')
            </div>
        </nav>
    </header>

    <header class="mobile">
        <div class="center">
            <a href="{{ route('home') }}" class="logo">{{ config('app.name') }}</a>
            <button id="mobile-toggle" type="button" role="button">
                <span class="lines"></span>
            </button>
        </div>
    </header>

    <div id="nav-mobile">
        <div class="center">
            @include('frontend.common.nav')
            <div class="social">
                @foreach(['instagram', 'facebook', 'linkedin'] as $s)
                    @if($contato->{$s})
                    <a href="{{ $contato->{$s} }}" target="_blank" class="{{ $s }}">{{ $s }}</a>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
