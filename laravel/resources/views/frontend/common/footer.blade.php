    <footer>
        <div class="atendimento">
            <div class="center">
                <div class="operario"></div>
                <p>
                    <span>Atendimento eficiente, direto e transparente.</span>
                    <span>Level1 Engenharia: sua melhor opção para reformar e construir.</span>
                </p>
            </div>
        </div>

        <div class="informacoes">
            <div class="center">
                <div class="sobre">
                    <img src="{{ asset('assets/img/layout/marca-level1-rodape.png') }}" alt="">
                    <p>{!! $contato->descricao !!}</p>
                    <div class="social">
                        @foreach(['instagram', 'facebook', 'linkedin'] as $s)
                            @if($contato->{$s})
                            <a href="{{ $contato->{$s} }}" target="_blank" class="{{ $s }}">{{ $s }}</a>
                            @endif
                        @endforeach
                    </div>
                </div>
                <div class="links">
                    <a href="{{ route('home') }}">HOME</a>
                    <a href="{{ route('quemSomos') }}">QUEM SOMOS</a>
                    <a href="{{ route('servicos') }}">NOSSOS SERVIÇOS</a>
                    <a href="{{ route('projetos') }}">PROJETOS</a>
                    <a href="{{ route('contato') }}">CONTATO</a>
                </div>
                <div class="informacoes-contato">
                    <form action="{{ route('newsletter') }}" id="form-newsletter">
                        <p>Cadastre-se para receber novidades</p>
                        <div class="box">
                            <input type="email" name="newsletter_email" placeholder="e-mail" required>
                            <button></button>
                        </div>
                    </form>
                    <h3>CONTATO</h3>
                    @if($contato->telefone)
                    <p class="telefone">{{ $contato->telefone }}</p>
                    @endif
                    @if($contato->whatsapp)
                    <p class="whatsapp">
                        <a href="https://api.whatsapp.com/send?phone=55{{ preg_replace('/\D/', '', $contato->whatsapp) }}" target="_blank">
                        {{ $contato->whatsapp }}
                        </a>
                    </p>
                    @endif
                    <a href="mailto:{{ $contato->email }}" class="email">{{ $contato->email }}</a>
                    @if($contato->endereco)
                    <p class="endereco">{{ $contato->endereco }}</p>
                    @endif
                </div>
            </div>
        </div>

        <div class="copyright">
            <div class="center">
                <p>
                    © {{ date('Y') }} {{ config('app.name') }} - Todos os direitos reservados.
                    <span></span>
                    <a href="http://www.trupe.net" target="_blank">Criação de sites</a>:
                    <a href="http://www.trupe.net" target="_blank">TRUPE AGÊNCIA CRIATIVA</a>
                </p>
            </div>
        </div>
    </footer>
