<a href="{{ route('home') }}" @if(Tools::routeIs('home')) class="active" @endif>Home</a>
<a href="{{ route('quemSomos') }}" @if(Tools::routeIs('quemSomos')) class="active" @endif>Quem Somos</a>
<a href="{{ route('servicos') }}" @if(Tools::routeIs('servicos')) class="active" @endif>Nossos Serviços</a>
<a href="{{ route('projetos') }}" @if(Tools::routeIs('projetos')) class="active" @endif>Projetos</a>
<a href="{{ route('contato') }}" @if(Tools::routeIs('contato')) class="active" @endif>Contato</a>
