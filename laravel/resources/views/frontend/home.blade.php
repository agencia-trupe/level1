@extends('frontend.common.template')

@section('content')

    <div class="header-home">
        <div class="logo">
            <div class="center">
                <div class="logo-box">{{ config('app.name') }}</div>
                <div class="barra-superior">
                    <div class="social">
                        @foreach(['instagram', 'facebook', 'linkedin'] as $s)
                            @if($contato->{$s})
                            <a href="{{ $contato->{$s} }}" target="_blank" class="{{ $s }}">{{ $s }}</a>
                            @endif
                        @endforeach
                    </div>
                    @if($contato->telefone)
                    <p class="telefone">{{ $contato->telefone }}</p>
                    @endif
                    @if($contato->whatsapp)
                    <p class="whatsapp">
                        <a href="https://api.whatsapp.com/send?phone=55{{ preg_replace('/\D/', '', $contato->whatsapp) }}" target="_blank">
                        {{ $contato->whatsapp }}
                        </a>
                    </p>
                    @endif
                    <a href="mailto:{{ $contato->email }}" class="email">{{ $contato->email }}</a>
                </div>
            </div>
        </div>

        <div class="banners">
            @foreach($banners as $banner)
            <div class="slide" style="background-image:url({{ asset('assets/img/banners/'.$banner->imagem) }})">
                <div class="center">
                    <div class="box-laranja"></div>
                    <div class="texto">
                        <h1>{!! $banner->titulo !!}</h1>
                        <h2>{!! $banner->subtitulo !!}</h2>
                    </div>
                </div>
            </div>
            @endforeach

            <div class="pager-wrapper">
                <div class="center">
                    <div class="cycle-pager"></div>
                </div>
            </div>
        </div>

        <nav>
            @include('frontend.common.nav')
        </nav>
    </div>

    <div class="home">
        <div class="center">
            <div class="servicos">
                <div class="servicos-lista">
                    <span>Construção & Reforma</span>
                    <span>Demolição</span>
                    <span>Ar Condicionado</span>
                    <span>Drywall</span>
                    <span>Civil</span>
                    <span>Hidráulica</span>
                    <span>Impermeabilização</span>
                    <span>Revestimentos</span>
                    <span>Elétrica</span>
                    <span>Pintura</span>
                    <span>Limpeza</span>
                </div>
                <div class="servicos-texto">
                    <img src="{{ asset('assets/img/home/'.$home->imagem_servicos) }}" alt="">
                    <div>
                        <h3>{{ $home->titulo_servicos }}</h3>
                        <p>{!! $home->texto_servicos !!}</p>
                    </div>
                </div>
            </div>

            <div class="home-texto">
                <div class="imagens">
                    <img src="{{ asset('assets/img/home/'.$home->imagem_1) }}" alt="">
                    <img src="{{ asset('assets/img/home/'.$home->imagem_2) }}" alt="">
                </div>
                <div class="texto">
                    <h2>{{ $home->titulo }}</h2>
                    <h3>{!! $home->subtitulo !!}</h3>
                    {!! $home->texto !!}
                    <a href="{{ route('contato') }}" class="btn-cotacao">Faça sua cotação &raquo;</a>
                </div>
            </div>
        </div>
    </div>

    <div class="projetos-destaque">
        <div class="center">
            <h3>Confira alguns projetos realizados</h3>

            <div class="projetos-thumbs">
                @foreach($projetos as $projeto)
                <a href="#" data-galeria="{{ $projeto->id }}">
                    <img src="{{ asset('assets/img/projetos/'.$projeto->capa) }}" alt="">
                    <div class="overlay">
                        <h3>{{ $projeto->titulo }}</h3>
                        <p>{{ $projeto->descricao }}</p>
                    </div>
                </a>
                @endforeach
            </div>

            <a href="{{ route('projetos') }}">Confira o portfólio completo &raquo;</a>
        </div>
    </div>

    <div class="hidden">
        @foreach($projetos as $projeto)
            @foreach($projeto->imagens as $imagem)
            <a href="{{ asset('assets/img/projetos/imagens/'.$imagem->imagem) }}" class="fancybox" rel="{{ $projeto->id }}" data-titulo="{{ $projeto->titulo }}" data-descricao="{{ $projeto->descricao }}"></a>
            @endforeach
        @endforeach
    </div>

    <script>
        if(typeof window.getComputedStyle(document.body).mixBlendMode == 'undefined') {
            document.documentElement.className += " no-blend-mode";
        }
    </script>

@endsection
