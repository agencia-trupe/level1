@extends('frontend.common.template')

@section('content')

    <div class="main projetos">
        <div class="center">
            <h1>Projetos</h1>
            <div class="projetos-thumbs">
                @foreach($projetos as $projeto)
                <a href="#" data-galeria="{{ $projeto->id }}">
                    <img src="{{ asset('assets/img/projetos/'.$projeto->capa) }}" alt="">
                    <div class="overlay">
                        <h3>{{ $projeto->titulo }}</h3>
                        <p>{{ $projeto->descricao }}</p>
                    </div>
                </a>
                @endforeach
            </div>
        </div>
    </div>

    <div class="hidden">
        @foreach($projetos as $projeto)
            @foreach($projeto->imagens as $imagem)
            <a href="{{ asset('assets/img/projetos/imagens/'.$imagem->imagem) }}" class="fancybox" rel="{{ $projeto->id }}" data-titulo="{{ $projeto->titulo }}" data-descricao="{{ $projeto->descricao }}"></a>
            @endforeach
        @endforeach
    </div>

@endsection
