@extends('frontend.common.template')

@section('content')

    <div class="main quem-somos">
        <div class="sobre">
            <div class="center">
                <div class="texto">
                    <h1>Quem Somos</h1>
                    {!! $quemSomos->texto !!}
                </div>
                <img src="{{ asset('assets/img/quem-somos/'.$quemSomos->imagem) }}" alt="">
            </div>
        </div>

        <div class="equipe">
            <div class="center">
                <h2>Perfil da Equipe</h2>
                <div class="destaque">
                    @foreach($equipe['destaque'] as $destaque)
                    <div>
                        <img src="{{ asset('assets/img/equipe/destaque/'.$destaque->imagem) }}" alt="">
                        <div class="texto">
                            <h4>{{ $destaque->titulo }}</h4>
                            <p>{!! $destaque->texto !!}</p>
                        </div>
                    </div>
                    @endforeach
                </div>
                <div class="normal">
                    @foreach($equipe['normal'] as $normal)
                    <div>
                        <img src="{{ asset('assets/img/equipe/'.$normal->imagem) }}" alt="">
                        <div class="texto">
                            <h4>{{ $normal->titulo }}</h4>
                            <p>{!! $normal->texto !!}</p>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection
