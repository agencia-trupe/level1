@extends('frontend.common.template')

@section('content')

    <div class="main servicos">
        <div class="center">
            <div class="texto">
                <h1>Nossos Serviços</h1>
                <p>{!! $servicos->texto !!}</p>
                <img src="{{ asset('assets/img/servicos/'.$servicos->imagem) }}" alt="">
            </div>
            <div class="circulo-wrapper">
                <div class="circulo">
                    @include('frontend.servicos-infografico', compact('servicos'))
                </div>
                <div class="circulo-texto">
                    <h4>Construção & Reforma</h4>
                    <p>{!! $servicos->construcao_reforma !!}</p>
                </div>
            </div>
            <div class="servicos-mobile">
                <div>
                    <div class="icone icone-construcao">
                        <img src="{{ asset('assets/img/layout/ico-infografico-construcaoreforma.png') }}" alt="">
                    </div>
                    <h4>Construção & Reforma</h4>
                    <p>{!! $servicos->construcao_reforma !!}</p>
                </div>
                <div>
                    <div class="icone">
                        <img src="{{ asset('assets/img/layout/ico-infografico-demolicao.png') }}" alt="">
                    </div>
                    <h4>Demolição</h4>
                    <p>{!! $servicos->demolicao !!}</p>
                </div>
                <div>
                    <div class="icone">
                        <img src="{{ asset('assets/img/layout/ico-infografico-arcondicionado.png') }}" alt="">
                    </div>
                    <h4>Ar Condicionado</h4>
                    <p>{!! $servicos->ar_condicionado !!}</p>
                </div>
                <div>
                    <div class="icone">
                        <img src="{{ asset('assets/img/layout/ico-infografico-drywall.png') }}" alt="">
                    </div>
                    <h4>Dry Wall</h4>
                    <p>{!! $servicos->dry_wall !!}</p>
                </div>
                <div>
                    <div class="icone">
                        <img src="{{ asset('assets/img/layout/ico-infografico-civil.png') }}" alt="">
                    </div>
                    <h4>Civil</h4>
                    <p>{!! $servicos->civil !!}</p>
                </div>
                <div>
                    <div class="icone">
                        <img src="{{ asset('assets/img/layout/ico-infografico-hidraulica.png') }}" alt="">
                    </div>
                    <h4>Hidráulica</h4>
                    <p>{!! $servicos->hidraulica !!}</p>
                </div>
                <div>
                    <div class="icone">
                        <img src="{{ asset('assets/img/layout/ico-infografico-impermeabilizacao.png') }}" alt="">
                    </div>
                    <h4>Impermeabilização</h4>
                    <p>{!! $servicos->impermeabilizacao !!}</p>
                </div>
                <div>
                    <div class="icone">
                        <img src="{{ asset('assets/img/layout/ico-infografico-revestimentos.png') }}" alt="">
                    </div>
                    <h4>Revestimentos em Geral</h4>
                    <p>{!! $servicos->revestimentos_em_geral !!}</p>
                </div>
                <div>
                    <div class="icone">
                        <img src="{{ asset('assets/img/layout/ico-infografico-eletrica.png') }}" alt="">
                    </div>
                    <h4>Elétrica</h4>
                    <p>{!! $servicos->eletrica !!}</p>
                </div>
                <div>
                    <div class="icone">
                        <img src="{{ asset('assets/img/layout/ico-infografico-pintura.png') }}" alt="">
                    </div>
                    <h4>Pintura</h4>
                    <p>{!! $servicos->pintura !!}</p>
                </div>
                <div>
                    <div class="icone">
                        <img src="{{ asset('assets/img/layout/ico-infografico-limpeza.png') }}" alt="">
                    </div>
                    <h4>Limpeza Fina</h4>
                    <p>{!! $servicos->limpeza_fina !!}</p>
                </div>
            </div>
        </div>

        <div class="diferenciais">
            <div class="center">
                <h3>Nossos diferenciais</h3>
                <div class="diferenciais-lista">
                    @foreach($diferenciais as $diferencial)
                    <div>
                        <img src="{{ asset('assets/img/diferenciais/'.$diferencial->imagem) }}" alt="">
                        <p>{!! $diferencial->texto !!}</p>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection
