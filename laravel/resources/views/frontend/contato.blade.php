@extends('frontend.common.template')

@section('content')

    <div class="main contato">
        <div class="center">
            <h1>Contato</h1>

            <div class="informacoes">
                <p class="telefone">{{ $contato->telefone }}</p>
                @if($contato->whatsapp)
                <p class="whatsapp">
                    <a href="https://api.whatsapp.com/send?phone=55{{ preg_replace('/\D/', '', $contato->whatsapp) }}" target="_blank">
                    {{ $contato->whatsapp }}
                    </a>
                </p>
                @endif
                <p class="atendimento">{!! $contato->atendimento !!}</p>
                <a href="mailto:{{ $contato->email }}" class="email">{{ $contato->email }}</a>
                <div class="social">
                    @foreach(['instagram', 'facebook', 'linkedin'] as $s)
                        @if($contato->{$s})
                        <a href="{{ $contato->{$s} }}" target="_blank" class="{{ $s }}">{{ $s }}</a>
                        @endif
                    @endforeach
                </div>
            </div>

            <form action="{{ route('contato.post') }}" method="POST">
                {!! csrf_field() !!}

                <p>Compartilhe sua ideia, o seu projeto, e juntos planejaremos a melhor execução para a sua obra.</p>

                @if($errors->any())
                <div class="flash flash-erro">
                    @foreach($errors->all() as $error)
                    {!! $error !!}<br>
                    @endforeach
                </div>
                @endif

                @if(session('enviado'))
                    <div class="flash flash-sucesso">
                        Mensagem enviada com sucesso!
                    </div>
                @endif

                <input type="text" name="nome" placeholder="nome" value="{{ old('nome') }}" required>
                <input type="email" name="email" placeholder="e-mail" value="{{ old('email') }}" required>
                <input type="text" name="telefone" value="{{ old('telefone') }}" placeholder="telefone">
                <textarea name="mensagem" placeholder="mensagem" required>{{ old('mensagem') }}</textarea>
                <input type="submit" value="ENVIAR">
            </form>
        </div>
    </div>

@endsection
